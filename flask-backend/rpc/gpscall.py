#coding:utf-8

import os,sys
sys.path.insert(0, os.path.abspath(os.path.dirname(__file__)) + '/../')

import socket
import json
from rpccall import send_rpc_request
import setting

GPS = setting.gps_setting

db_addr = ('121.199.9.188', 60004)
BUFSIZE=1024

'''
GPS_CMD_RESET = 1
GPS_CMD_SHUTDOWN = 2
GPS_CMD_FIX = 3
GPS_CMD_FREQ = 4
GPS_HV_ALARM = 5
GPS_STATIC_ALARM = 6
GPS_SIM_SWITCH = 7
GPS_LIGHT_SWITCH = 8
GPS_SHAKE_ALARM = 9
GPS_FACTORY_RESET = 10
GPS_LOWPOWER_ALARM = 11
'''

def gps_reset(uuid):
    request_data = {}
    request_data["query"] = 'gpsservice'
    data = dict()
    data['uuid'] = uuid
    data['cmdtype'] = GPS['reset']
    request_data['data'] = data
    request_str = json.dumps(request_data)
    #return run_rpc(request_str)
    return send_rpc_request(request_str, db_addr)

def gps_shutdown(uuid):
    request_data = {}
    request_data["query"] = 'gpsservice'
    data = dict()
    data['uuid'] = uuid
    data['cmdtype'] = GPS['shutdown']
    request_data['data'] = data
    request_str = json.dumps(request_data)
    #return run_rpc(request_str)
    return send_rpc_request(request_str, db_addr)

def gps_fix(uuid):
    request_data = {}
    request_data["query"] = 'gpsservice'
    data = dict()
    data['uuid'] = uuid
    data['cmdtype'] = GPS['fix']
    request_data['data'] = data
    request_str = json.dumps(request_data)
    #return run_rpc(request_str)
    return send_rpc_request(request_str, db_addr)

def gps_frequency(uuid, freq):
    request_data = {}
    request_data["query"] = 'gpsservice'
    data = dict()
    data['uuid'] = uuid
    data['cmdtype'] = GPS['freq']
    data['frequency'] = freq
    request_data['data'] = data
    request_str = json.dumps(request_data)
    #return run_rpc(request_str)
    return send_rpc_request(request_str, db_addr)

def gps_hv_alarm(uuid, hv_speed, on_off):
    request_data = {}
    request_data["query"] = 'gpsservice'
    data = dict()
    data['uuid'] = uuid
    data['cmdtype'] = GPS['hv_alarm']
    data['hv_speed'] = hv_speed
    data['on_off'] = on_off
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return send_rpc_request(request_str, db_addr)

def gps_static_alarm(uuid, on_off):
    request_data = {}
    request_data["query"] = 'gpsservice'
    data = dict()
    data['uuid'] = uuid
    data['cmdtype'] = GPS['static_alarm']
    data['on_off'] = on_off
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return send_rpc_request(request_str, db_addr)

def gps_sim_switch(uuid, sim_no):
    request_data = {}
    request_data["query"] = 'gpsservice'
    data = dict()
    data['uuid'] = uuid
    data['cmdtype'] = GPS['sim_switch']
    data['sim_no'] = sim_no
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return send_rpc_request(request_str, db_addr)

def gps_light_switch(uuid, on_off):
    request_data = {}
    request_data["query"] = 'gpsservice'
    data = dict()
    data['uuid'] = uuid
    data['cmdtype'] = GPS['light_switch']
    data['no_off'] = on_off
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return send_rpc_request(request_str, db_addr)

def gps_shake_alarm(uuid, on_off):
    request_data = {}
    request_data["query"] = 'gpsservice'
    data = dict()
    data['uuid'] = uuid
    data['cmdtype'] = GPS['shake_alarm']
    data['no_off'] = on_off
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return send_rpc_request(request_str, db_addr)

def gps_factory_reset(uuid):
    request_data = {}
    request_data["query"] = 'gpsservice'
    data = dict()
    data['uuid'] = uuid
    data['cmdtype'] = GPS['factory_reset']
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return send_rpc_request(request_str, db_addr)

def gps_lowpower_alarm(uuid, on_off):
    request_data = {}
    request_data["query"] = 'gpsservice'
    data = dict()
    data['uuid'] = uuid
    data['cmdtype'] = GPS['lowpower_alarm']
    data['on_off'] = on_off
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return send_rpc_request(request_str, db_addr)

def gps_set_efence(uuid, lng, lat, radius):
    request_data = {}
    request_data["query"] = 'gpsservice'
    data = dict()
    data['uuid'] = uuid
    data['cmdtype'] = GPS['set_efence']
    data['lng'] = lng
    data['lat'] = lat
    data['radius'] = radius
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return send_rpc_request(request_str, db_addr)

def gps_enable(uuid):
    request_data = {}
    request_data["query"] = 'gpsservice'
    data = dict()
    data['uuid'] = uuid
    data['cmdtype'] = GPS['enable_gps']
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return send_rpc_request(request_str, db_addr)

def gps_unable(uuid):
    request_data = {}
    request_data["query"] = 'gpsservice'
    data = dict()
    data['uuid'] = uuid
    data['cmdtype'] = GPS['unable_gps']
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return send_rpc_request(request_str, db_addr)

'''
time 中断间隔
sensityivity 灵敏度 (0-137)
'''
def gps_set_a_sensor(uuid, time, sensitivity):
    request_data = {}
    request_data["query"] = 'gpsservice'
    data = dict()
    data['uuid'] = uuid
    data['cmdtype'] = GPS['set_a_sensor']
    data['time'] = time
    data['sensitivity'] = sensitivity
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return send_rpc_request(request_str, db_addr)

def gps_get_config(uuid):
    request_data = {}
    request_data["query"] = 'gpsservice'
    data = dict()
    data['uuid'] = uuid
    data['cmdtype'] = GPS['get_config']
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return send_rpc_request(request_str, db_addr)

def gps_hot_start(uuid):
    request_data = {}
    request_data["query"] = 'gpsservice'
    data = dict()
    data['uuid'] = uuid
    data['cmdtype'] = GPS['hot_start']
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return send_rpc_request(request_str, db_addr)



if __name__ == "__main__":
    #uuid = '867400033137930'
    uuid = '867378038785277'    #simcom
    #uuid = '867717032853013'   #ofo
    lng = '120.271505'
    lat = '30.195728'
    radius = 100
    ret = gps_frequency(uuid, 1) #0 gps设置成低功耗模式 10 gps设置成非低功耗模式
    #ret = gps_fix(uuid)
    #ret = gps_set_efence(uuid, lng, lat, radius)
    #ret = gps_enable(uuid)
    #ret = gps_unable(uuid)
    #ret = gps_get_config(uuid)
    #ret = gps_set_a_sensor(uuid, 100, 120)
    #ret = gps_hot_start(uuid)
    print(ret[0])
    print(ret[1])
