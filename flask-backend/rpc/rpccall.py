#coding:utf-8

import os,sys
sys.path.insert(0, os.path.abspath(os.path.dirname(__file__)) + '/../')

import socket
import json
from db.pack import *


db_addr = ('121.199.9.188',55558)
#db_addr = ('121.199.9.188', 60004)
BUFSIZE=1024

def rpc_opendoor(uuid):
    request_data = {}
    request_data["query"] = 'rpcservice'
    data = dict()
    data['uuid'] = uuid
    data['cmdtype'] = 0
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return run_rpc(request_str)

def rpc_closedoor(uuid):
    request_data = {}
    request_data["query"] = 'rpcservice'
    data = dict()
    data['uuid'] = uuid
    data['cmdtype'] = 1
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return run_rpc(request_str)

def rpc_stopdoor(uuid):
    request_data = {}
    request_data["query"] = 'rpcservice'
    data = dict()
    data['uuid'] = uuid
    data['cmdtype'] = 2
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return run_rpc(request_str)

def run_rpc(data):
    err = dict()
    s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    s.settimeout(4)
    try:
        s.connect(db_addr)
    except(Exception):
        err['errcode'] = -1
        err['errmsg'] = 'connect failed'
        ret = (-1, str(err))
        
    else:
        send_data = enpack(data)
        try:
            s.send(send_data.encode('ISO-8859-1'))
            recv_data = s.recv(2).decode('ISO-8859-1')
            while(header != recv_data):
                recv_data = s.recv(2).decode('ISO-8859-1')
            data_len = s.recv(4).decode('ISO-8859-1')
            data_len = unpack_len(data_len)
            len_temp = 0
            feedback = b''
            while(len_temp!=data_len):
                feedback_temp=s.recv(data_len-len_temp)
                feedback += feedback_temp
                len_temp += len(feedback_temp)
        except(Exception):
        #except(BlockingIOError):
            #ret = (-1,"timeout")
            err['errcode'] = -1
            err['errmsg'] = 'timeout'
            ret = (-1, str(err))
            return ret
        else:
            ret = (0,feedback.decode('utf-8'))
    s.close()
    return ret

'''
def send_rpc_request(data, addr):
    err = dict()
    s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    s.settimeout(4)
    try:
        s.connect(addr)
    except(Exception):
        err['errcode'] = -1
        err['errmsg'] = 'connect failed'
        ret = (-1, str(err))
        
    else:
        send_data = enpack(data)
        try:
            s.send(send_data.encode('ISO-8859-1'))
            recv_data = s.recv(2).decode('ISO-8859-1')
            while(header != recv_data):
                recv_data = s.recv(2).decode('ISO-8859-1')
            data_len = s.recv(4).decode('ISO-8859-1')
            data_len = unpack_len(data_len)
            len_temp = 0
            feedback = b''
            while(len_temp!=data_len):
                feedback_temp=s.recv(data_len-len_temp)
                feedback += feedback_temp
                len_temp += len(feedback_temp)
        except(Exception):
        #except(BlockingIOError):
            #ret = (-1,"timeout")
            err['errcode'] = -1
            err['errmsg'] = 'timeout'
            ret = (-1, str(err))
            return ret
        else:
            ret = (0,feedback.decode('utf-8'))
    s.close()
    return ret
'''
def send_rpc_request(data, addr):
    err = dict()
    s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    s.settimeout(4)
    try:
        s.connect(addr)
    except(Exception):
        err['errcode'] = -1
        err['errmsg'] = 'connect failed'
        ret = (-1, str(err))
    else:
        send_data = enpack(data)
        try:
            s.send(send_data.encode('ISO-8859-1'))
            recv_data = s.recv(2).decode('ISO-8859-1')
            if ( len(recv_data) == 0):
                err['errcode'] = -1
                err['errmsg'] = 'peer socket closed'
                ret = (-1, str(err))
                return ret
            
            if ( header != recv_data ):
                err['errcode'] = -1
                err['errmsg'] = 'invalid header data'
                ret = (-1, str(err))
                return ret

            data_len = s.recv(4).decode('ISO-8859-1')
            data_len = unpack_len(data_len)
            len_temp = 0
            feedback = b''
            while(len_temp!=data_len):
                feedback_temp=s.recv(data_len-len_temp)
                if (len(feedback_temp) == 0):
                    print('recv nothing')
                    break
                feedback += feedback_temp
                len_temp += len(feedback_temp)
        except(Exception):
            err['errcode'] = -1
            err['errmsg'] = 'timeout'
            ret = (-1, str(err))
            return ret
        else:
            ret = (0,feedback.decode('utf-8'))
    s.close()
    return ret


if __name__ == "__main__":
    #uuid = '888888888888888'
    uuid = '867717032853013'
    ret = rpc_closedoor(uuid)
    print(ret[0])
    print(ret[1])
    
