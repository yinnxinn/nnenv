import requests
import json
import logging
from app.envs.data import info

'''
try:
    from .config.data import info
except:
    from config.data import info
'''

logger = logging.getLogger('info')

# 检查用户是否认证身份, 如果认证过，同时返回身份
def checkCerti(openid):
    # TODO
    import random
    ids = random.randint(0,2)    
    if random.randint(0,2) % 2 ==0:
        return True, ids
    else:
        return False, None



def code2openid(code):
    tmp = {
        'code':code,
        "appid":info.get('appid'),
        "appsecret":info.get('appsecret')
    }
    # 从微信服务器换取openid
    from app.envs.urls import code2openid
    url = code2openid.format(**tmp)
    response = requests.get(url)

    result = json.loads(response.text)
    logger.info('获取用户id ' + str(result))
    openid = ''
    if 'openid' in result:
        openid = result.get('openid')
    else:
        logger.warning('使用code换取用户openid发生错误')

    return openid