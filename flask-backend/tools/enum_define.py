
from enum import Enum



class ENUMURLCODE(Enum):
    #数据库内部错误
    db_error                    = -1    #数据库内部错误
    #常规
    success                     = 0     #成功
    failed                      = -1    #失败
    #用户认证   
    token_error                 = 101   #token认证失败，无效密钥
    token_expire                = 102   #token认证过期
    token_no_openid             = 103   #token中没有openid
    token_openid_not_register   = 104   #token通过认证，但数据库中没有相关用户的注册信息
    #微信用户注册
    wechat_no_code              = 201   #用户注册没有获得code
    wechat_openid_failed        = 202   #从微信服务器获取openid失败
    wechat_no_openid            = 203   #从微信服务器获取数据中不含openid
    #用户登录
    login_no_openid             = 205   #用户登录时无token
    #数据库访问
    db_conn_timeout             = 301   #数据库连接失败
    db_query_timeout            = 302   #数据库请求超时
    db_datatype_error           = 303   #数据库请求数据不是规定的类型（python字典）
    db_datafield_miss           = 304   #数据库返回数据不是规定的格式（缺少字段）
  
