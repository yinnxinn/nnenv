

import os,sys
sys.path.insert(0, os.path.abspath(os.path.dirname(__file__)) + '/../')

import json
from querydb import request_db




def createGPSUser(userid, passwd):
    request_data = {}
    request_data['query'] = 'createGPSUser'
    data = dict()
    data['userID'] = userid
    data['passWord'] = passwd
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return request_db(request_str)

def createGPSDevice(imei, group):
    request_data = {}
    request_data['query'] = 'createGPSDevice'
    data = dict()
    data['imei'] = imei
    data['group'] = group
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return request_db(request_str)

def bindGPSDevice(imei, userid):
    request_data = {}
    request_data['query'] = 'bindGPSDevice'
    data = dict()
    data['imei'] = imei
    data['userID'] = userid
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return request_db(request_str)

def unbindGPSDevice(imei, userid):
    request_data = {}
    request_data['query'] = 'unbindGPSDevice'
    data = dict()
    data['imei'] = imei
    data['userID'] = userid
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return request_db(request_str)

def getGPSDevices(userid):
    request_data = {}
    request_data['query'] = 'getGPSDevices'
    data = dict()
    data['userID'] = userid
    request_data['data'] = data
    request_str = json.dumps(request_data)
    return request_db(request_str)










if __name__ == "__main__":
    userid = '13326130699'
    passwd = '123456'
    #ret = createGPSUser(userid, passwd)
    imei = '888888888888888'
    group = 0
    #ret = createGPSDevice(imei, 0)
    #ret = bindGPSDevice(imei, userid)
    ret = getGPSDevices(userid)
    #ret = unbindGPSDevice(imei, userid)
    print(ret[0])
    print(ret[1])

