
import struct
header = "\x0b\x0d"
lens = 0xffffffff
#数据各字节异或校验
def check_str(data):
    temp = [ord(t) for t in data]
    check=temp[0]
    for it in temp[1:]:
        check ^=it
    check_s = chr(check)
    return check_s

#对数据打包
#格式：头 + 长度 + 数据
def enpack(data):
    data_len = len(data)
    data_len = lens & data_len
    byte_len = struct.pack('!i',data_len)
    #byte_len = data_len.to_bytes(4,byteorder='big',signed=True)
    #temp_data = bytes.decode(byte_len)+data
    temp_data = str(byte_len,'ISO-8859-1')
    temp_data += data
    temp_data = header + temp_data
    return temp_data

#从数据长度bytes中取得数据长度
def unpack_len(bdata):
    data_byte = bytes(bdata,encoding='ISO-8859-1')
    data_len = struct.unpack('!i',data_byte)
    #data_len = int.from_bytes(data_byte,byteorder='big',signed=True)
    return data_len[0]

if __name__ == "__main__":
    
    ret = enpack('你好你好你好你好你好你好你好你好你好你好你好你好你好你好好')
    print(ret)
    ret_1 = unpack_len(ret[2:6])
    print(ret_1)
