# 回复用户话术
ReplyMsg = {
    "touser": "openid", 
    "msgtype": "text", 
    "text": {
        "content": "Hello World"
    }
}

if __name__ == '__main__':
    import requests
    from urls import sendText
    headers = {'Content-Type': 'application/json;charset=UTF-8'}
    ReplyMsg['touser'] = 'oY-MZv-GRm6ArAfpwhkxGg4LMsFw'
    result = requests.post(sendText.format(**{'token': '33_ae2gycKF8AjuzQVPmFBoI8isNDrHagbOJtf-GKXn6DavxIHV3YpchhZlHg0F3eC7n0U1HQYvVZPjp8BWrjcLfpW8C7PLHSXcGLTC93QJM0jtSFHbXOLwB3-4Xy98lDp9ChPrmtUV1JVChKoXGIIcABARTT'}), headers=headers, data=ReplyMsg)
    print(result.text)
