# 获取token
gtokenUrl = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={appid}&secret={appsecret}'
#创建菜单
createMenuURL= 'https://api.weixin.qq.com/cgi-bin/menu/create?access_token={token}'
#查询菜单
checkMenu = 'https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info?access_token={token}'
# 添加客服账号
addKef = 'https://api.weixin.qq.com/customservice/kfaccount/add?access_token={token}'
# 修改客服
modifyKef = 'https://api.weixin.qq.com/customservice/kfaccount/update?access_token={token}'
# 删除客服
delKef = 'https://api.weixin.qq.com/customservice/kfaccount/del?access_token={token}'
# 群发消息
sendAllOnTag = 'https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token={token}'
# 模版消息
setIndustry = 'https://api.weixin.qq.com/cgi-bin/template/api_set_industry?access_token={token}'
# 获取模版id
getTemplateId = 'https://api.weixin.qq.com/cgi-bin/template/api_add_template?access_token={token}'
# 获取全部模版
getTemplate = 'https://api.weixin.qq.com/cgi-bin/template/get_all_private_template?access_token={token}'
# 发送模版消息
sendTemplateMsg = 'https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={token}'
# 创建用户标签
creatUsrTag = 'https://api.weixin.qq.com/cgi-bin/tags/create?access_token={token}'
#获取用户标签
getUsrTag = 'https://api.weixin.qq.com/cgi-bin/tags/get?access_token={token}'
# 编辑用户标签
editUsrTag = 'https://api.weixin.qq.com/cgi-bin/tags/update?access_token={token}'
# 获取用户信息
getUsrInfo = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token={token}&openid={openid}&lang=zh_CN'
# 批量获取用户
getAllUsr = 'https://api.weixin.qq.com/cgi-bin/user/get?access_token={token}&next_openid={openid}'
# 获取用户地址
getUsrAdd = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token={token}&openid={openid}&lang=zh_CN'
#批量获取用户信息
getAllUsrInfo = 'https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token={token}'
# 临时二维码
tmpQR = 'https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token={token}'
# 永久二维码
QR = 'https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token={token}'
# 用户数据
getUsrData = 'https://api.weixin.qq.com/datacube/getusersummary?access_token={token}'
# 累计用户数据
allUsrData = 'https://api.weixin.qq.com/datacube/getusercumulate?access_token={token}'
# 网页授权以后使用code换取token和openid
code2openid = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid={appid}&secret={appsecret}&code={code}&grant_type=authorization_code'
# 获取js_ticket
getJsTicket = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token={token}&type=jsapi"
# 向用户发送消息
sendText = 'https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={token}'

