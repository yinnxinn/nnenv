from tools.enum_define import ENUMURLCODE
from tools.utils import code2openid
from db.querydb import request_db_param


# 获取设备信息
def getDeviceInfo(dict_data):
    #TODO
    result = dict()
    area = dict_data.get('area')
    deviceid = dict_data.get('deviceid')
    code = dict_data.get('code')
    if ( code is None or deviceid is None):
        result['errcode'] = ENUMURLCODE.failed.value
        result['errmsg'] = 'missing params'
        return result
    openid = code2openid(code)
    if (openid is None):
        result['errcode'] = ENUMURLCODE.login_no_openid.value
        result['errmsg'] = 'wechat token failed'
        return result
    
    params = dict()
    params['deviceid'] = deviceid
    ret = request_db_param('nn_getdevicelist', params)
    return ret[1]


# 获取社区信息
def getCommunityInfo(dict_data):
    #TODO
    result = dict()
    area = dict_data.get('area')
    code = dict_data.get('code')
    if ( code is None or area is None ):
        result['errcode'] = ENUMURLCODE.db_datafield_miss.value
        result['errmsg'] = 'missing params'
        return result
    openid = code2openid(code)
    if (openid is None):
        result['errcode'] = ENUMURLCODE.login_no_openid.value
        result['errmsg'] = 'wechat token failed'
        return result
    
    params = dict()
    params['area'] = area
    ret = request_db_param('nn_getcommunityInfo', params)
    return ret[1]

def openDoor(dict_data):
    from rpc.rpccall import rpc_opendoor
    #TODO
    result = dict()
    deviceid = dict_data.get('deviceid')
    code = dict_data.get('code')
    if ( code is None or deviceid is None):
        result['errcode'] = ENUMURLCODE.db_datafield_miss.value
        result['errmsg'] = 'missing params'
        return result
    openid = code2openid(code)
    if (openid is None):
        result['errcode'] = ENUMURLCODE.login_no_openid.value
        result['errmsg'] = 'wechat token failed'
        return result
    ret = rpc_opendoor(deviceid)
    return ret[1]

def closeDoor(dict_data):
    #TODO
    from rpc.rpccall import rpc_closedoor
    result = dict()
    deviceid = dict_data.get('deviceid')
    code = dict_data.get('code')
    if ( code is None or deviceid is None):
        result['errcode'] = ENUMURLCODE.db_datafield_miss.value
        result['errmsg'] = 'missing params'
        return result
    openid = code2openid(code)
    if (openid is None):
        result['errcode'] = ENUMURLCODE.login_no_openid.value
        result['errmsg'] = 'wechat token failed'
        return result
    ret = rpc_closedoor(deviceid)
    return ret[1]


def pauseCmd(dict_data):
    #TODO
    from rpc.rpccall import rpc_stopdoor
    result = dict()
    deviceid = dict_data.get('deviceid')
    code = dict_data.get('code')
    if ( code is None or deviceid is None):
        result['errcode'] = ENUMURLCODE.db_datafield_miss.value
        result['errmsg'] = 'missing params'
        return result
    openid = code2openid(code)
    if (openid is None):
        result['errcode'] = ENUMURLCODE.login_no_openid.value
        result['errmsg'] = 'wechat token failed'
        return result
    
    ret = rpc_stopdoor(deviceid)
    return ret[1]
