





gps_setting = {
    'reset': 1,
    'shutdown': 2,
    'fix': 3,
    'freq': 4,
    'hv_alarm': 5,
    'static_alarm': 6,
    'sim_switch': 7,
    'light_switch': 8,
    'shake_alarm': 9,
    'factory_reset': 10,
    'lowpower_alarm': 11,
    'set_efence': 12,
    'enable_gps': 13,
    'unable_gps': 14,
    'set_a_sensor': 15,
    'get_config': 16,
    'hot_start': 17
}