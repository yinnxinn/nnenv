import app

from app.main import createApp

app = createApp()

# 本地启动
if __name__ == '__main__':
    app.run(host='0.0.0.0', port='1000', debug=True)
